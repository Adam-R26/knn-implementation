#COMP 219 AI Assignment 1
#201271021
from sklearn.neighbors import KNeighborsClassifier
from sklearn import datasets
from sklearn.model_selection import train_test_split
import joblib
from sklearn.metrics import accuracy_score

data = datasets.load_digits()
X_train, X_test, y_train, y_test = train_test_split(data.data, data.target, test_size=0.20, random_state=0)
knn = KNeighborsClassifier(n_neighbors = 1)#Library model initalised here
knn.fit(X_train,y_train)#Library model trained here
predictions = knn.predict(X_test)#Library models returns predictions about the test data here 


def F1():#Method encasing all requirements of F1
    stepper = 0
    numOfEntries = len(data.data)
    numOfClassifiers = len(data.target_names)
    print('Number of data entries:', numOfEntries)
    print('Number of classifiers', numOfClassifiers)
    classifierTrack = [0,0,0,0,0,0,0,0,0,0]
    for i in data.target:#Step through the array holding the numbers that correspond to the pixels in .data
        classifierTrack[i] = classifierTrack[i]+1#Count number of entries for each classifier.
        
    for j in range(10):#Loop to display entry number for each classifier.
        print('Entries for classifier',j,":",classifierTrack[j])
        j = j+1

    for m in range(64):#Loop to find max minimum values for each feature.
        maximum = data.data[stepper][stepper]
        minimum = data.data[stepper][stepper]
        for n in range(len(data.data)):
            if(data.data[n][m]>maximum):
                maximum = data.data[n][m]
            
            elif(data.data[n][m]<minimum):
                minimum = data.data[n][m]
                
        print("The max value for feature:",m+1,"is:",maximum)
        print("The min value for feature",m+1,"is",minimum)
        m = m+1;
        stepper = stepper + 1

    print('The training data is:', X_train)
    print('The test data is:', X_test)

    
def F2():#Saves trained library implementation model and outputs its accuracy as required for F4()
    joblib.dump(knn,'library_knn')
    accuracyLibraryKNN()
    
    
def F3():#Saves my own KNN model using joblib.
    myKNN = KNN(X_train,y_train,X_test,y_test)
    joblib.dump(myKNN,'library_MyKNN')#Saving my trained model. 
    
def F4():#Runs my implementation of KNN on all of the training data and compares results to actual values to calculate accuracy. Outputs this accuracy and the library accuracy along with errors.
    myKNN = KNN(X_train,y_train,X_test,y_test)
    print("The accuracy for the library implementation is:"+" "+str(accuracyLibraryKNN())+"%")
    errorLib = 100-accuracyLibraryKNN()
    print("Therefore error in the model is:"+" "+str(errorLib)+"%")
    errorMyKNN = 100-myKNN.predictAll()
    print("Therefore the error in my model is:"+" "+str(errorMyKNN))
    
    
def F5():
    myKNN = KNN(X_train,y_train,X_test,y_test)
    print("Here are some predictions of random indexs of the test data to show my F5 working")
    myKNN.predict(10)
    myKNN.predict(20)
    myKNN.predict(30)
    myKNN.predict(40)
    print("You can change the indexs of the query data if you wish to try different indexs just go to function F5.")
    
    
def accuracyLibraryKNN():#Outputs accuracy of library KNN model without having to run F2 or F3.
    accuracy = accuracy_score(y_test, predictions)
    return accuracy*100
    
    
class KNN:#A class for my own implementation of KNN
    def __init__(self,X,y,X2,y2):
        self.X_train = X
        self.y_train = y
        self.X_test = X2
        self.y_test = y2
        
    def predictAll(self):
        correctGuesses = 0
        euclideanArray = [] #Array to hold euclidean distances for each number tested
        neighbours = []#Array to hold k nearest neighbours of each test instance.
        guesses = []#Array to hold predictions of each test instance.
        
        for i in range(len(self.X_test)):
            euclideanArray.append(self.euclideanDist(self.X_train,self.X_test[i]))
        
        for i in range(len(euclideanArray)):
            neighbours.append(self.getNeighbours(1,euclideanArray[i]))
            
        for i in range(len(neighbours)):
            guesses.append(self.getResponse(neighbours[i],self.y_train))
            
        for i in range(len(guesses)):
            if(guesses[i] == self.y_test[i]):
                correctGuesses = correctGuesses + 1
                
        return self.accuracy(correctGuesses,guesses)
        
    def euclideanDist(self,X_train,X_test):#Parameters: X_train 2D array of 1794 entries, X_test 1D array of an instance
        distances = []
        sumOfDistances = 0
        for x in range(len(X_train)): 
            for i in range(64):
                sumOfDistances = sumOfDistances + (((X_train[x][i]-X_test[i])**2)**0.5)
            
            distances.append(sumOfDistances)
            sumOfDistances = 0
        return distances
    
    def getNeighbours(self,k, distances):
        neighbours = []
        for i in range(k):#Make K passes to find k nearest neighbours
            smallest = distances[0]
            smallestIndex = 0
            for j in range(len(distances)):#Loop through distances array and find smallest element
                if(smallest>distances[j]):
                    smallestIndex = j
                    smallest = distances[j]
                
            neighbours.append(smallestIndex)#Save index for predictions
            distances[smallestIndex] = 10000 #Make entry arbitrarily large so that next loop doesn't pick it up again.
            
        return neighbours
    
    def getResponse(self,neighbours,y_train):
        predictions = []
        predictionCount = [0,0,0,0,0,0,0,0,0,0]
        for i in range(len(neighbours)):
            predictions.append(self.y_train[(neighbours[i])])
        
            
        for i in range(len(predictions)):
            predictionCount[(predictions[i])] = predictionCount[(predictions[i])]+1
            
        projected = predictionCount[0]
        prediction = 0;
        for i in range(len(predictionCount)):
            if(projected<predictionCount[i]):
                projected = predictionCount[i]
                prediction = i
                
        return prediction
    
    def accuracy(self,correctGuesses,numOfGuesses):#Takes the array containing all correct guesses so that we can find difference between that number and num of entries to calc accuracy.
        accuracy = (correctGuesses/(len(numOfGuesses)))*100
        print('The accuracy for my implementation is: '+str(accuracy)+"%")
        return accuracy
    
    def predict(self, index):#Fullfills the requirements of F5
        distances = self.euclideanDist(self.X_train,self.X_test[index])
        neighbours = self.getNeighbours(1,distances)
        prediction = self.getResponse(neighbours,self.y_train)
        print("We predict that entry",index,"in the model is a number:",prediction)
        
F1()
F4()
F5()
        





                        
    

          

